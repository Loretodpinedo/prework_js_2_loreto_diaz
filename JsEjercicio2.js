//Debes crear una función que sepa clasificar ruedas de juguete, siempre y cuando le indiques el diámetro de la misma ⚙️
//En base al diámetro que reciba la función se comportará así:
//Si el diámetro es inferior o igual a 10, la función deberá imprimir por consola "es una rueda para un juguete pequeño"
//Si el diámetro es superior a 10 y menor de 20, la función deberá imprimir por consola "es una rueda para un juguete mediano"
//Si el diámetro es superior o igual a 20, la función deberá imprimir por consola "es una rueda para un juguete grande"

function diametroRuedaJuguete(diametro){

    if (diametro <= 10){
        console.log("Es una rueda para un jugte pequeño");
    }else if ( diametro > 10 && diametro < 20){
            console.log("Es una rueda para un juguete mediano");
        }else if(diametro > 20 || diametro == 20){
        console.log("Es una rueda para un juguete grande");

    }
}

diametroRuedaJuguete(4);
diametroRuedaJuguete(15);
diametroRuedaJuguete(45);

